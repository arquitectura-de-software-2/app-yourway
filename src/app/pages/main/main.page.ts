import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  selectedPath = ''

  constructor(
    private router: Router
  ) { 
    this.router.events.subscribe( (event: RouterEvent) => {
      this.selectedPath = event.url
    })
  }

  async ngOnInit() {
  }

}
