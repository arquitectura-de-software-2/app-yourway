import { Component, OnInit } from '@angular/core';
import { SportMan } from 'src/app/models/sportman';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: SportMan = {
    id:1,
    name: 'Naruto',
    lastName: 'Uzumaki',
    avatar: 'https://media1.tenor.com/images/4cc16b834723bbf4fec152454480d4e3/tenor.gif?itemid=9203508',
    birthDay: '16 Jul 2020',
    mail: 'uzumaki@konoha.com'
  }
  constructor() { }

  ngOnInit() {
  }

}
