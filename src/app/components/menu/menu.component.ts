import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service'
import { Observable } from 'rxjs';
import { MenuOption } from 'src/app/interfaces/interfaces'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  menuOptions:  Observable<MenuOption[]>

  constructor(private dataService: DataLocalService) { }

  ngOnInit() {
    this.menuOptions = this.dataService.getMenu()
  }

}
