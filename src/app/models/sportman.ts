export interface SportMan {
    id: number;
    name: string;
    lastName: string;
    mail: string;
    avatar: string;
    birthDay: string;
}