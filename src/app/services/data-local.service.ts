import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { MenuOption } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  constructor(private http: HttpClient) { }

  getMenu(){
    return this.http.get<MenuOption[]>('/assets/data/menu.json')
  }
}
