import { TestBed } from '@angular/core/testing';

import { SportmanService } from './sportman.service';

describe('SportmanService', () => {
  let service: SportmanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SportmanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
