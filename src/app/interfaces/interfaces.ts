export interface MenuOption {
    icon: string;
    name: string;
    redirectTo: string;
}